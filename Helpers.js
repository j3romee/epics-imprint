var Helpers = {
    calculateRealDistance : function(pointA,pointB){
            var R = 6371e3,     // Radius of earth in meter
                lat1 = pointA.lat,
                lat2 = pointB.lat,
                lon1 = pointA.lng,
                lon2 = pointB.lng,
                phi1 = this.toRadians(lat1),
                phi2 = this.toRadians(lat2),
                delta1 = this.toRadians(lat2-lat1),
                delta2 = this.toRadians(lon2-lon1);

            var a = Math.sin(delta1/2) * Math.sin(delta1/2) +
                Math.cos(phi1) * Math.cos(phi2) *
                Math.sin(delta2/2) * Math.sin(delta2/2);

            return R * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));        // Return distance in meters
    },
    calculateInputDistance: function(inputDestination){
        return Math.round(Math.sqrt(Math.pow(inputDestination.lat,2) + Math.pow(inputDestination.lng,2)));
    },
    toRadians: function(nb){
        return nb * Math.PI / 180;
    },
    toDegrees: function(nb){
        return nb * 180 / Math.PI;
    },
    transformCoordinates : function(offset){
        // Offset are given in meters
        var R = 6371e3,     // Radius of earth in meter
            initialLocation = MapManager.config.initialLocation,
            dLat = (Config.unitDistance *offset.lat)/ R,
            dLng = (Config.unitDistance*offset.lng)/(R*Math.cos(Helpers.toRadians(initialLocation.lat))),
            newLat = initialLocation.lat + Helpers.toDegrees(dLat),
            newLng = initialLocation.lng + Helpers.toDegrees(dLng);

        return {lat : newLat, lng : newLng};
    }
};