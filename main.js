// TODO : Remove google logo ?
// TODO : Disable user zoom ? Only the app should zoom and unzoom
// TODO : Unlocked mode
// TODO : Add link to emus website (And facebook ?)


$(document).ready(function(){
    MapManager.initMap();

    $("#calculate-distance-submit-btn").on('click',function(){
        if(MapManager.markerMovementTimer == undefined) {
            MapManager.drawPointWithDistance();
        }
    });
    $("#inform-police-btn").on('click',function(){
        if(MapManager.markerMovementTimer == undefined) {
            var searchModal = $(".modal.search");
            $("#do-travel-submit-btn").on('click', function () {
                MapManager.policeMove();
            });
            MapManager.cleanMap();
            searchModal.modal();
        }
    });
});

function doAction(e){
    // TODO : Limité nombre de tentative (Reset de tentative avec mot de passe ?)
    var inputDestination = {
        lat : $("#travel-lat").val(),
        lng : $("#travel-lng").val()
    };

    var realDestination = Helpers.transformCoordinates(inputDestination);

    if(validateDestination(realDestination)){
        MapManager.travelRoute(realDestination);
    }else{
        alert("We don't have enough fuel to go there !");
    }
}

function lockControls(){
    $("#distance-control").fadeOut(500);
    $("#travel-control").fadeOut(500);
}

function unlockControls(){
    $("#distance-control").fadeIn(500);
    $("#travel-control").fadeIn(500);
}

function validateDestination(destination){
    var dist = Helpers.calculateRealDistance(MapManager.config.initialLocation,destination);

    return dist < 4000;
}