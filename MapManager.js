var MapManager = {
    map : undefined,
    currentPositionMarker : undefined,
    markerMovementTimer: undefined,

    config : {
        mapContainer : "#map",
        initialLocation : {
            lat : 45.378192,
            lng : -71.926299
        },
        route :{
            strokeColor : '#131540',
            strokeOpacity : 0.6,
            strokeWeight : 6
        },
        moveMarkerDelay : 150

    },

    initMap : function(){
        var mapHeight = window.innerHeight - $('.emus-header').outerHeight();
        $("#map-container").css('height',mapHeight);
        this.map = new GMaps({
            el: this.config.mapContainer,
            lat: this.config.initialLocation.lat,
            lng: this.config.initialLocation.lng,
            disableDefaultUI: true,
            zoom : 13
        });

        // Put the UDES logo on the map
        var icon = new google.maps.MarkerImage('/udes-letter.png',
            new google.maps.Size(80, 80), //size
            new google.maps.Point(0, 0), //origin point
            new google.maps.Point(40, 40)); // offset point

        this.map.addMarker({
            lat: this.config.initialLocation.lat,
            lng: this.config.initialLocation.lng,
            icon: icon,
            zIndex: 10
        });
    },

    drawPointWithDistance: function(markerPos){
        var inputDestination = {
                lat : $("#distance-lat").val(),
                lng : $("#distance-lng").val()
            };

        if(inputDestination.lat != "" && inputDestination.lng != "") {
            if(/[a-z]/i.test(inputDestination.lat) || /[a-z]/i.test(inputDestination.lng)){
                alert("Les coordonnées doivent être en format décimal !");
            }else {
                MapManager.cleanMap();

                $("#distance-lat").val("");
                $("#distance-lng").val("");

                var distance = Helpers.calculateInputDistance(inputDestination),
                    realDestination = Helpers.transformCoordinates(inputDestination);

                MapManager.drawRoute(realDestination);

                var watchdog = setTimeout(function () {
                    // TODO : Better Error Handling
                    alert("Une erreur inconnue s'est produite, veuillez en informer un tuteur");
                }, 900);

                MapManager.map.getRoutes({
                    origin: [MapManager.config.initialLocation.lat, MapManager.config.initialLocation.lng],
                    destination: [realDestination.lat, realDestination.lng],
                    callback: function (routes) {
                        clearTimeout(watchdog);
                        var endlocation = routes[0].legs[0].end_location;

                        var bounds = new google.maps.LatLngBounds();
                        bounds.extend(MapManager.map.markers[0].getPosition());
                        bounds.extend(endlocation);

                        MapManager.map.fitBounds(bounds);
                        if (MapManager.map.getZoom() > 16) {
                            MapManager.map.setZoom(16);
                        } else {
                            MapManager.map.zoomOut(1);
                        }

                        // Add distance overlay over the coordinate
                        MapManager.map.drawOverlay({
                            lat: endlocation.lat(),
                            lng: endlocation.lng(),
                            verticalAlign: 'top',
                            horizontalAlign: 'center',
                            content: "<div class='overlay'>Distance : " + distance + " m<div class='overlay_arrow above'></div></div>"
                        });
                    }
                });
            }
        }else{

        }
    },

    drawRoute: function(dest){
        // TODO : Change color of route
        this.map.drawRoute({
            origin: [this.config.initialLocation.lat, this.config.initialLocation.lng],
            destination: [dest.lat, dest.lng],
            travelMode: 'WALKING',
            strokeColor : "#f4ac27",
            strokeOpacity : 0.8,
            strokeWeight : 6
        });
    },

    recenterMap: function(){
        MapManager.map.setCenter(MapManager.config.initialLocation.lat,MapManager.config.initialLocation.lng);
    },

    cleanMap : function(){
        MapManager.recenterMap();

        // Clean routes
        this.map.removePolylines();

        // Clean movement timer
        if(MapManager.markerMovementTimer){
            clearTimeout(MapManager.markerMovementTimer);
            MapManager.markerMovementTimer = undefined;
        }

        // Clean movement events
        $("#map").off("markerMovementFinished");

        // Clean overlays
        MapManager.map.removeOverlays();

        // Clean markers
        while(MapManager.map.markers.length > 1){
            MapManager.map.removeMarker(MapManager.map.markers[1]);
        }
    },

    onPoliceMoveFinish: function(destination){
        // TODO : Lock everything (Hide the menus, only show a place to enter a password)
        if(destination.lat == Config.positionMoto.lat && destination.lng == Config.positionMoto.lng){
            $(".moto-found").modal("show");
        }else{
            $(".moto-not-found").modal("show");
        }
    },

    policeMove: function(e){
        var base = $(".base-selector").val(),
            inputDestination = {
                lat : $("#travel-lat").val(),
                lng : $("#travel-lng").val()
            };

        if(inputDestination.lat != "" && inputDestination.lng != "") {
            if(base == 16 && (/[g-z]/i.test(inputDestination.lat) ||/[g-z]/i.test(inputDestination.lng)) ){
                alert("Les coordonnées entrées ne sont pas dans le bon format !");
            }else if(base == 8 && (/[8-9a-z]/i.test(inputDestination.lat) || /[8-9a-z]/i.test(inputDestination.lng))){
                alert("Les coordonnées entrées ne sont pas dans le bon format !");
            }else if(base == 2 && (/[2-9a-z]/i.test(inputDestination.lat) || /[2-9a-z]/i.test(inputDestination.lng))){
                alert("Les coordonnées entrées ne sont pas dans le bon format !");
            }else {
                MapManager.cleanMap();

                $("#do-travel-submit-btn").off('click');

                $("#travel-lat").val("");
                $("#travel-lng").val("");

                $(".modal.search").modal("hide");

                inputDestination.lat = parseInt(inputDestination.lat, base);
                inputDestination.lng = parseInt(inputDestination.lng, base);
                var realDestination = Helpers.transformCoordinates(inputDestination);

                MapManager.drawRoute(realDestination);

                var watchdog = setTimeout(function () {
                    // TODO : Better Error Handling
                    alert("Une erreur inconnue s'est produite, veuillez en informer un tuteur");
                }, 900);

                MapManager.map.getRoutes({
                    origin: [MapManager.config.initialLocation.lat, MapManager.config.initialLocation.lng],
                    destination: [realDestination.lat, realDestination.lng],
                    callback: function (routes) {
                        clearTimeout(watchdog);
                        var endlocation = routes[0].legs[0].end_location;

                        var bounds = new google.maps.LatLngBounds();
                        bounds.extend(MapManager.map.markers[0].getPosition());
                        bounds.extend(endlocation);

                        MapManager.map.fitBounds(bounds);
                        if (MapManager.map.getZoom() > 16) {
                            MapManager.map.setZoom(16);
                        } else {
                            MapManager.map.zoomOut(2);
                        }

                        var icon = new google.maps.MarkerImage('/x-mark-small-2.png',
                            new google.maps.Size(80, 80), //size
                            new google.maps.Point(0, 0), //origin point
                            new google.maps.Point(30, 40)); // offset point

                        MapManager.map.addMarker({
                            lat: endlocation.lat(),
                            lng: endlocation.lng(),
                            icon: icon,
                            zIndex: 10
                        });


                        var markerImage = new google.maps.MarkerImage('/popo2.png',
                            new google.maps.Size(80, 80), //size
                            new google.maps.Point(0, 0), //origin point
                            new google.maps.Point(40, 40)); // offset point

                        var policeMarker = MapManager.map.addMarker({
                            lat: routes[0].overview_path[1].lat(),
                            lng: routes[0].overview_path[1].lng(),
                            icon: markerImage,
                            zIndex: 40
                        });

                        $("#map").one("markerMovementFinished", function () {
                            MapManager.onPoliceMoveFinish(inputDestination)
                        });

                        var moveMarker = function (index) {
                            if (index < routes[0].overview_path.length) {

                                MapManager.map.setCenter(routes[0].overview_path[index].lat(), routes[0].overview_path[index].lng());

                                policeMarker.setPosition(routes[0].overview_path[index]);

                                MapManager.markerMovementTimer = setTimeout(function () {
                                    moveMarker(index + 1);
                                }, MapManager.config.moveMarkerDelay);
                            } else {
                                MapManager.markerMovementTimer = undefined;
                                $("#map").trigger("markerMovementFinished");
                            }
                        };

                        MapManager.markerMovementTimer = setTimeout(function () {
                            moveMarker(0);
                            MapManager.map.setZoom(16);
                        }, 1000);
                    }
                });
            }
        }else{
            // TODO : Show error message
        }
    },

    // TODO : Clean this
    cleanRoute: function(){
        for (var i = 0; i < this.map.polylines.length; i++) {
            this.map.polylines[i].setMap();                   // Set undefined map to polyline element, removing them from the map
        }
    },
    // Old version
    travelRoute: function(dest){
        this.cleanRoute();
        this.map.travelRoute({
            origin: [this.config.initialLocation.lat, this.config.initialLocation.lng],
            destination: [dest.lat, dest.lng],
            travelMode: 'walking',
            step: function(e){
                $('body').append('<li>'+e.instructions+'</li>');
                setTimeout(function() {
                    MapManager.map.drawPolyline({
                        path          : e.path,
                        strokeColor   : MapManager.config.route.strokeColor,
                        strokeOpacity : MapManager.config.route.strokeOpacity,
                        strokeWeight  : MapManager.config.route.strokeWeight
                    });
                    MapManager.map.setCenter(e.lat_lngs[0].lat(),e.lat_lngs[0].lng());      // Center map around the new point
                },500* e.step_number);
            }
        });
    },

};